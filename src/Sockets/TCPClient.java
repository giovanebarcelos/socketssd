package Sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import javax.swing.JOptionPane;

public class TCPClient {

	public static void main(String[] args) {
		final String URL = "8.tcp.ngrok.io";
		final int PORT = 15344;
		
		int qtyMessages = 0;				
		while (true) {
			String message = JOptionPane.showInputDialog("Digite um mensagem");
			
			try {
				Socket socket = new Socket(URL, PORT);
				qtyMessages++;
				
				System.out.println(qtyMessages+") Conectado a porta "+PORT+" da URL "+URL);
				
				OutputStream os = socket.getOutputStream();
				ObjectOutputStream objOS = new ObjectOutputStream(os);
				
				System.out.println("Enviando a mensagem para o servidor!");
				objOS.writeObject(message);
				
				InputStream is = socket.getInputStream();
				ObjectInputStream objIS = new ObjectInputStream(is);
				
				String response = (String) objIS.readObject();
				System.out.println("Mensagem recebida: "+response);
								
				socket.close();
				
			} catch (IOException e) {
				System.out.println("Erro de conexão: "+e.getMessage());
			} catch (ClassNotFoundException e) {
				System.out.println("Erro de recebimento: "+e.getMessage());
			}
		}
	}

}
