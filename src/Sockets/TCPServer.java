package Sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		final int PORT = 9300;		
		ServerSocket ss = new ServerSocket(PORT);
		
		System.out.println("Escutando a porta "+PORT);			
		
		int qtyConnections = 0;
		while (true) {
			Socket socket = ss.accept();			
			qtyConnections++;
			
			System.out.println(qtyConnections+") Conexão aceita, ativada e aguardando!");
			
			InputStream is = socket.getInputStream();
			ObjectInputStream objIS = new ObjectInputStream(is);
			
			String message = (String) objIS.readObject();
			
			System.out.println("Mensagem: "+message);
			
			OutputStream os = socket.getOutputStream();
			ObjectOutputStream objOS = new ObjectOutputStream(os);
			
			objOS.writeObject("OK! Mensagem recebida com sucesso!");
			socket.close();
		}
	}
}
