#!/usr/bin/env python
import pika, sys, os

def main():
    url = os.environ.get('CLOUDAMQP_URL', 'amqps://hjunxhly:uQCS8qD1x2IG94dtF1qxbT9me9SLNF6Y@cattle.rmq2.cloudamqp.com/hjunxhly')
    params = pika.URLParameters(url)

    connection = pika.BlockingConnection(params)
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body.decode())

    channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
