#!/usr/bin/env python
import pika, os

url = os.environ.get('CLOUDAMQP_URL', 'amqps://hjunxhly:uQCS8qD1x2IG94dtF1qxbT9me9SLNF6Y@cattle.rmq2.cloudamqp.com/hjunxhly')
params = pika.URLParameters(url)

connection = pika.BlockingConnection(params)
channel = connection.channel()

channel.queue_declare(queue='hello')

message = input("Message to send: ")

channel.basic_publish(exchange='', routing_key='hello', body=message)
print(" [x] Sent 'Hello World!'")
connection.close()
