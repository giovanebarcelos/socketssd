package Thread;

public class ThreadApp extends Thread{

	private int tempoDeEspera;

	public ThreadApp(String nomeThread, int tempoDeEspera) {
		super(nomeThread);
		
		this.tempoDeEspera = tempoDeEspera;
		
		System.out.println("Thread "+nomeThread+" criada e vair dormir por "+
		this.tempoDeEspera/1000 + " seg");
	}
	
	@Override
	public void run() {
		try {
			Thread.sleep(this.tempoDeEspera);			
		} catch (InterruptedException e) {
			System.err.println(e.toString());
		}
		
		System.out.println("Thread "+ this.getName() + " acordando para calcular os números da mega-sena");
		
		int numerosDaMegaSena[] = getNumerosDaMegaSena();
		
		System.out.print(this.getName()+") Os números da mega sena são: ");
		for (int numeroMegaSena = 0; numeroMegaSena < numerosDaMegaSena.length; numeroMegaSena++) {
			System.out.print(numerosDaMegaSena[numeroMegaSena]);
			System.out.print(numeroMegaSena == numerosDaMegaSena.length-2 ? " e ":
				numeroMegaSena == numerosDaMegaSena.length-1 ? "": ", " );
		}
		System.out.println();
	}

	private int[] getNumerosDaMegaSena() {
		int numerosDaMegaSena[] = new int[6],		
		    qtdeNumeros = 5; 
		
		while (qtdeNumeros > -1) {
			numerosDaMegaSena[qtdeNumeros] = (int) (Math.random() * 60)+1;
			qtdeNumeros--;
		}
		
		return numerosDaMegaSena;
	}
}
