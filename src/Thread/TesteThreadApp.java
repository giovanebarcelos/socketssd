package Thread;

public class TesteThreadApp {

	public static void main(String[] args) {
		ThreadApp threads[] = new ThreadApp[30];
		
		System.out.println("Criando as Threads!\n");
		
		for (int threadNum = 0; threadNum < threads.length; threadNum++) {
			threads[threadNum] = new ThreadApp("Thread "+threadNum, 
					(int) (Math.random() * 100000)); 		
		}
		
		System.out.println("Iniciando as Threads!\n");
		
		for (int threadNum = 0; threadNum < threads.length; threadNum++) {
			threads[threadNum].start(); 		
		}
	}
}
