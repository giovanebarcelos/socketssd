package RMI;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class RMIClient {
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		RMIInterface rmiMath = (RMIInterface) Naming.lookup("//localhost/RMIMath");
		
		System.out.println("Soma 13 + 8 = " + rmiMath.somar(13, 8));
		System.out.println("Multiplicação 13 * 8 = " + rmiMath.multiplicar(13, 8));
		System.out.println("O fatorial de 5 é " + rmiMath.fatorial(5));
	}
}
