package RMI;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class RMIServer extends UnicastRemoteObject implements RMIInterface{	
	private static final long serialVersionUID = 1L;

	protected RMIServer() throws RemoteException {
		super();
	}

	public Float somar(float num1, float num2) {
		System.out.println("Somar");
		return num1 + num2;		
	}
	
	public Float multiplicar(float num1, float num2) {
		System.out.println("Multiplicar");
		return num1 * num2;		
	}
	
	public int fatorial(int numero) {
		System.out.println("Fatorial");
		return (numero < 2) ? 1 : numero * fatorial(numero-1);
	}

	private static void loadServer(int port) throws RemoteException {
		java.rmi.registry.LocateRegistry.createRegistry(port);
	}
	
	public static void main(String[] args) throws RemoteException, MalformedURLException, AlreadyBoundException {
		RMIServer.loadServer(1099);
		
		RMIServer rmis = new RMIServer();
		
		Registry registry = LocateRegistry.getRegistry();		
		registry.rebind("RMIMath", rmis);	
		
		System.out.println("Servidor esta ativo!");
	}
}
