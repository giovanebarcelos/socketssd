package RMI;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIInterface extends Remote{
	public Float somar(float num1, float num2) throws RemoteException;	
	public Float multiplicar(float num1, float num2) throws RemoteException;
	public int fatorial(int numero) throws RemoteException;
}
